import React, { Component } from "react";
import PropTypes from "prop-types";

const Book = (book) => {
  const hasCoverImage = !!book.coverImageUrl || book.coverImageUrl.length !== 0;
  const classes = book.expanded ? "book__more-info expanded" : "book__more-info collapsed";
  return <div className="book" onClick={book.toggleExpand}>
      {!hasCoverImage ? (
        <i className="fas fa-book fa-5x book__cover--icon" />
      ) : (
        <img src={book.coverImageUrl} alt="book-icon" className="book__cover" />
      )}
      <p className="book__title">
        <a href={book.url}>{book.title}</a>
      </p>
      <p className="book__author">{book.author}</p>
      <p>{book.shortDescription}</p>
      <div className={classes}>
        <p>{book.publicationDate}</p>
        <p>{book.publisher}</p>
        <p>{book.detailedDescription}</p>
      </div>
    </div>;
};

Book.propTypes = {
  title: PropTypes.string.isRequired
};

export default Book;