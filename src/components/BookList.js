import React, { Component } from "react";
import Book from './Book';
import books from "../books.js";

class BookList extends Component {
  constructor () {
    super();
    this.state = {
      books: books,
      expanded: false
    }
  }

  toggleExpandSection = () => {
    debugger;
   if (this.state['expanded']) {
     this.setState({expanded: false});
   } else {
     this.setState({expanded: true});
   }
  }

  render() {
    let books = this.state['books'];
    return (
      books.map((book, idx) =>
        <Book
          title = {book.title}
          author = {book.author}
          url = {book.url}
          coverImageUrl = {book.coverImageUrl}
          shortDescription =  {book.shortDescription}
          publisher = {book.publisher}
          publicationDate = {book.publicationDate}
          detailedDescription = {book.detailedDescription}
          expanded = {book.expanded}
          toggleExpand = {this.toggleExpandSection}
          key = {idx}
        />
      )
    )
  }
}

export default BookList;