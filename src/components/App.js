import React, { Component } from 'react';
import BookList from './BookList';
import '../styles/App.css';

class App extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div className="container">
        <section className="booklist">
          <BookList />
        </section>
      </div>
    );
  }
}

export default App;
